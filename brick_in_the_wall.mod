MODULE vasMainModule
  record rowColPos
    NUM row;
    NUM col;
  endrecord

  PERS tooldata TOUL := [TRUE,[[0,0,100],[1,0,0,0]],[1,[5,5,-50],[1,0,0,0],0,0,0]];
	VAR zonedata ZONNE := [FALSE,2,3,3,0.3,3,0.3];
	VAR speeddata SPEEED := [200,500,5000,1000];

	CONST orient orient1 := [0,1,0,0];
	
  ! fixed stations st1 on top of stack
  !st2 on top of wall
	VAR POS st1 := [3000,1780,1100];
	VAR POS st2 := [6000,1780,1100];
	
	! reaching positions / variable positions
	VAR POS pStack := [3000,1780,1100];
	VAR POS pWall := [6000,1780,1100];

	CONST confdata CONF1 := [-0.5,0.5,0.5,0.5];
	
	CONST extjoint ExAx1 := [2000,9E9,9E9,9E9,9E9,9E9];
  CONST extjoint ExAx2 := [4712.21,9E9,9E9,9E9,9E9,9E9];
  
	VAR bool isHoldingBrick := FALSE;
	VAR num counter:=0;
  VAR num numOfBricks :=0;
  CONST num brickHeight := 75;
  CONST ROBTARGET posSt1 := [st1,orient1, CONF1, ExAx1];
	CONST ROBTARGET posSt2 := [st2,orient1, CONF1, ExAx1];
	VAR ROBTARGET posInStack := [pStack,orient1, CONF1, ExAx1];
	VAR ROBTARGET posInWall := [pWall,orient1, CONF1, ExAx1];

	! brickBool turns true when there is a brick
	! that is when the plunger sends an I/o signal
  VAR bool brickBool;
  VAR num blah;
  
  ! make a data tree
  VAR NUM chkList := [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

  VAR NUM indexToRow :=0; ! row for the index
  VAR NUM indexToCol :=0; ! col for the index

  ! initalize data for current brick row and col
  VAR rowColPos currentRowCol := [ 0,0];
  
  ! checking pos , this is the position which is currently
  ! being checked for correctness
  VAR NUM checkRow :=0;
  VAR NUM checkCol :=0;
  

  ! inital start X
  VAR NUM startX :=3000;
  
  ! number of bricks for even row
  VAR NUM bricksPerRow :=5;
  
  ! chkListBool declaration
  VAR BOOL chkListBool :=TRUE;
  

  

  ! this is the main procedure to be called
	PROC theMainProc()
		COnfJ \off;
		ConfL \off;
		! pWall to 1,1 ( 1st row 1st col)
		pWallToRowCol cBrRow,cBrCol;
    ! and an update in positions
    updatePositions;
    
    TPWrite "hello how are you doing ? ";
    
		getNumOfBricks;
		WHILE (counter<numOfBricks) DO
			repeatProc;
			counter := counter + 1;
            TPWrite "counter incremented" \num:=counter;
        ENDWHILE
    ENDPROC

	PROC getNumOfBricks()
        TPReadNum numOfBricks, "How many bricks are to be stacked";
        TPReadNum bricksPerRow, " How many bricks to be placed per row??";
    ENDPROC

	PROC repeatProc()
      ! moves to the brick in stack
		MoveL posInStack,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
    brickBool := isThereABrick; ! here the brick bool is assigned with checked value.
    ! the underneath while loop is to go further down till it fetches a brick.
		WHILE not(brickBool) DO
            TPWrite " A Brick in stack is Missing ";
			      pStack.z := pStack.z - brickHeight;
            updatePositions;
            MoveL posInStack,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
            
            !isthereabrick function changes brickBool
            isThereABrick;
        ENDWHILE
		brickManipulate; ! catch the brick
		 ! moves to station 1 that is above stack
		MoveL st1,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
    ! moves to station 2
		MoveL st2,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
     ! now it is time to move to row 1 and col 1 which is the initial starting point
     
    pWallToRowCol currentRowCol;
    
    MoveL posInWall,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
		!read I/o bool ??
    brikBool := isThereABrick;
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! this while loop needs to be coded
    !! for error check
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    

    
		WHILE NOT( brickBool ) DO ! checklist vals shuld all be true till the brickplaced
		  ! get the row col to check
		  ! check for presence by going there
		  ! update data tree   ( done )
		  ! update brickBool - check if the previous from
      !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    TPWrite " A Brick in wall is Missing ";
            
    ! get which position to check next based on the true false data tree
    updatePositions;
            
    MoveL pos4,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
            
    brickBool := isThereABrick;
            
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ENDWHILE
        
		brickManipulate;
   ! after placing the brick move out of place to station 2 and then station 1
		MoveL st2,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
		MoveL st1,SPEEED,ZONNE,TOUL \Wobj:=WObj0;
		
		! now change the values of the posInStack and posInWall. next iteration
		! shall call the move command to go to the stack and it continues
    TPReadFK blah, "starting to change vals", stEmpty,stEmpty,stEmpty,"ok","ok"; ! debugging print statement just here to debug
    setCToNxtRowCol; !updates cBrRow , cBrCol numbers to next row, col
    
    
    pWallToRowCol cBrRow,cBrCol; ! changes pWall robtarget to specified row col. here it is cBrRow cBrCol which are just updated in last line
    pStack.z := P1.z + brickHeight;! still considering stack is not a dynamic. fetches next brick
    updatePositions;

    ENDPROC
    FUNC NUM getIndexToChk( NUM row, NUM col )
         !goes thru the array list and checks for 0.
         
    ENDFUNC
         
         


    PROC updateDataTree(NUM row, NUM col)
         ! updates the data tree with 1s starting  at row, col
         ! call this proc upon finding a true value at row
         VAR NUM i :=1;
         VAR NUM j :=1;
         VAR NUM index;
         
         IF row MOD 2 =1 THEN
            initColOffset := 1;
         ELSE
            initColOffset := 0;
         ENDIF
         

         WHILE i<=row DO

           WHILE j<=i DO
             index := rowColToIndex(row - i+1,col-1+j-(initColOffset MOD 2 ));
             chkList{index} := 1;   ! check for assignment syntax here  how to assign to arrays ??
             j := j +1;
           ENDWHILE
           i := i +1;
           initColOffset := initColOffset + 1;
         ENDWHILE

      ENDPROC
       


  FUNC BOOL isThereABrick()
      VAR BOOL brickbool;
      TPReadFK reg1, "is there a brick ?", stEmpty, stEmpty, stEmpty, "yes :) ", "NO";
      IF reg1=4 THEN
          brickBool := TRUE;
      ELSE
          brickBool := FALSE;
      ENDIF
      RETURN brickBool;
  ENDFUNC


	PROC brickManipulate()
		IF isHoldingBrick THEN
			TPReadFK blah, "Imagine Brick caught ", stEmpty, stEmpty, stEmpty, "yes :) ", "NO";
		ELSE
			TPReadFK blah, "Imagine Brick released ", stEmpty, stEmpty, stEmpty, "yes :) ", "NO";
		ENDIF
		isHoldingBrick := NOT (isHoldingBrick);
	ENDPROC

! the positions are not references that
! auto update when p1,p2,p3,p4 are updated
! so manually these robtargets need to be updated.

  PROC updatePositions()
    ! pos in stack represents the position in stack
    posInStack := [pStack,orient1, CONF1, ExAx2];
    ! pos in wall represents pos in Wall based on row and col
    posInWall := [pWall,orient1, CONF1, ExAx2];
  ENDPROC
  
  PROC checkBrAtRowCol(NUM row, NUM col)
       ! the checking position of robot shuld be above the brick
       MoveL st2,SPEEED,ZONNE,TOUL \Wobj:=WObj;


       MoveL st2,SPEEED,ZONNE,TOUL \Wobj:=WObj;
  ENDPROC




  FUNC NUM rowColToIndex(NUM row, NUM col)
       ! turns row, col sent into index number of the checklist
       VAR NUM rowColIndex;

       IF row MOD 2 =1 THEN
          rowColIndex := (row -1)/2*(2*bricksPerRow -1) + col;
       ELSE
          rowColIndex := (2*bricksPerRow -1)*(row/2 -1) + bricksPerRow + col;
       ENDIF
       
       RETURN rowColIndex;
       
   ENDFUNC
   
  PROC indexToRowCOl(NUM index)
       VAR NUM di :=  index DIV (2*bricksPerRow -1); ! will need to do integer divison here
       VAR NUM rem := index MOD (2*bricksPerRow -1); ! will need to do modulus remainder here
       
       IF rem>bricksPerRow THEN
         indexToRow := (di+1)*2;
         indexToCol := rem - bricksPerRow;
       ELSE
         indexToRow := di*2+1;
         indexToCol := rem;
       ENDIF
         
  ENDPROC


  PROC setCToPrevRowCol()
       ! this proc maniupates cBrRow and cBrCol to assign previous row and previous col
       VAR NUM row := cBrRow;
       VAR NUM col := cBrCol;
       
       IF col<=1 THEN
         cBrRow := row -1;
         cBrCol := bricksPerRow;
       ELSE
         cBrRow := row;
         cBrCol := col-1;
       ENDIF
       
       IF cBrRow<=0 THEN
         cBrRow :=1;
         cBrCol := 1;
       ENDIF
       
     ENDPROC

     
  PROC setCToNextRowCol()
       ! this function sets the cBrRow and cBrCol to next row and next col numbers
       
       VAR NUM row := cBrRow;
       VAR NUM col := cBrCol;

       IF col <= bricksPerRow THEN
         cBrRow := row +1;
         cBrCol := 1;
       ELSE
         cBrRow := row;
         cBrCol := col+1;
       ENDIF

     ENDPROC

    
    ! this function updates of p4 values and also
    ! calls update positions to assign the values to
    ! rob target pos4
  FUNC null pWallToRowCol(rowColPos currentRC) ! check if rapid allows viod func else convert this to proc
       ! changes p4 values and calls update
       
        VAR NUM row := currentRC.row;
        VAR NUM col := currentRc.col;

        pWall.z := baseHeight + brHeight/2 + brHeight*row;
        pWall.x := startX + brWidth/2 + brWidth*col;
        ! find modulo operator here
        !*****
        IF row MOD 2 = 1 THEN
          pWall.x:= pWall.x + brWidth/2;
        ENDIF
        updatePositions;
  ENDFUNC
          
  !!! personal math functions below




ENDMODULE

